<?php

use Illuminate\Database\Seeder;


class OurupdateTableSeeder extends Seeder
{
  
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    
	{
	     DB::table('ourupdate')->insert(
            [
                
                [ 
                    'title' => 'Income Increase Shows the Recovery Is Very Much Real',
                    'contents' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit sed do eiusmod tempor incididunt laboris nisi ut aliquip ex ea commodo consequat...',
                    'image' => "public/frontend/assets/image/post-thumb-a.jpg",
                    'is_published'          =>    1,
                ],
                 [ 
                    'title' => 'An Economics Nobel awarded for Examining Reality',
                    'contents' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit sed do eiusmod tempor incididunt laboris nisi ut aliquip ex ea commodo consequat...',
                    'image' => "public/frontend/assets/image/post-thumb-b.jpg",
                    'is_published'          =>    1,
                ],
                 [ 
                    'title' => 'Maybe Supply-Side Economics Deserves a Second Look',
                    'contents' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit sed do eiusmod tempor incididunt laboris nisi ut aliquip ex ea commodo consequat...',
                    'image' => "public/frontend/assets/image/post-thumb-c.jpg",
                    'is_published'          =>    1,
                ],
               

            ]);

	}
}
