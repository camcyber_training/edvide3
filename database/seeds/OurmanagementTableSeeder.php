<?php

use Illuminate\Database\Seeder;


class OurmanagementTableSeeder extends Seeder
{
  
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    
	{
	     DB::table('ourmanagement')->insert(
            [
                
                [ 
                    'title' => 'Kunthel OUM',
                    'contents' => 'Managing Director',
                    'image' => "public/frontend/assets/image/team/team-a.jpg",
                    'is_published'          =>    1,
                ],
                [ 
                    'title' => 'Dolla PHA',
                    'contents' => 'Education Dept Manager',
                    'image' => "public/frontend/assets/image/team/team-b.jpg",
                    'is_published'          =>    1,
                ],
                [ 
                    'title' => 'Eiza CHHUN',
                    'contents' => 'Visa Service Dept Manager',
                    'image' => "public/frontend/assets/image/team/team-c.jpg",
                    'is_published'          =>    1,
                ],
                [ 
                    'title' => 'Molyka Brasoeur',
                    'contents' => 'Education Edviser',
                    'image' => "public/frontend/assets/image/team/team-d.jpg",
                    'is_published'          =>    1,
                ],
               

            ]);

	}
}
