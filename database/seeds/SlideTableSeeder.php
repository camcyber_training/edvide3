<?php

use Illuminate\Database\Seeder;


class SlideTableSeeder extends Seeder
{
  
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    
	{
	     DB::table('slides')->insert(
            [
                
                [ 
                    'title' => 'Expert Study Advice',
                    'image' => "public/frontend/assets/image/slider-lg-a.jpg",
                    'is_published'          =>    1,
                ],
                [ 
                    'title' => 'Business Consulting',
                    'image' => "public/frontend/assets/image/slider-lg-b.jpg",
                    'is_published'          =>    1,
                ],
                
               

            ]);

	}
}
