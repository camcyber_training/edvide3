<?php

use Illuminate\Database\Seeder;

class WhoweareTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('whoweare')->insert(
            [
                [
                    'title'                 => 'WHO WE ARE',
                    'name'                  => 'Mr. Kunthel Oum / Managing Director',
                    'contents'              => 'Edvise is founded by a group of young and dynamic Cambodian entrepreneurs who have more than a decade of experiences in education, travelling & visa arrangement and career counseling. Edvise is one of the most reliable visa services, academic and overseas education consultants in Cambodia. We focus on assisting Cambodians in achieving their wishes for visa grants to their targeted destinations.',
                    'image'                 => 'public/frontend/assets/image/photo-home-a.jpg',
                    'is_published'          =>    1,
                ],
                
               


                
            ]
        );
    }
}
