<?php

use Illuminate\Database\Seeder;


class ServiceSpecificTableSeeder extends Seeder
{
  
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    
	{
	     DB::table('service_specific')->insert(
            [
                
                [ 
                    'title' => 'Study abroad',
                    'content'=>'The advantages of overseas education are very high in numbers nowadays, since it facilitates better exposures in wider horizons. Obtaining a globally accepted degree from abroad allows people to travel and live anywhere in the world with highflying careers, a truly cosmopolitan experience.


Education and Visa Service (Edvise) provides overall support from pre-application to admission processes. Edvise offers a time-tested and cost effective service package for students to ensure successful admissions.


Edvise has long-standing experiences in connecting aspiring students with educational institutions across the globe. Today, we can place students in any country from Australia to France for any programs from Astronomy to Zen Buddhism. Distances are no more barriers for aspiring students as we lead a global life through Edvise.


Admissions Information
Edvise counselors help students in admission process by advising them of required documentations and assisting them with the necessary application forms and evidences. Edvise counselors work closely with admission staff at oversea institutions and help resolve unusual queries for our students. We also help students in gaining credits for their prior equivalent educational qualifications. Students applying through Edvise can also avail application fee waivers and get spot admission offers*.

General Admission Requirement
Documentation you’ll need to apply:


-A completed and signed school or university application form

-A certified copy of your academic results, including full academic transcripts

-A certified copy of your passport’s personal details page

-TOEFL score taken within the last 24 months with an overall Score of 525 / 197 / 70 and above with the minimum validity of 6 months at the time of applying. OR

-IELTS score taken within the last 24 months with an overall score of 5.5/ 6.0, with the minimum validity of 6 months at the time of applying. If the student doesn’t satisfy the above criteria for English, he will need to study ESL.

-Personal Statement (Essay)

-Letter of Recommendation: Students are required to submit at least one recommendation letter, preferably from a faculty member under who has taught the student in college / school.

-If original documents aren’t in English, they’ll need to supply certified translations as well as the documents.

Counselors
Edvise counselors are trained in counseling students for multiple destinations and at all levels of education. Edvise is one of the few agencies that sends its counselors overseas to be trained within institutions on programs, student experiences and lifestyles on campuses.


A large number of our counselors and managers have teaching backgrounds and therefore, have an in-depth understanding of student needs and career aspirations. A number of our counselors and their family members have been educated overseas which helps them guide students through their personal experiences. Edvise counselors have expertise to offer you Admission counseling in - school level programs, diploma, undergraduate, postgraduate and doctoral level programs.

Our counselors will advise you on Visa requirements and help you in every step till you get your visa.


The following are the counseling protocols within E that every counselor must undertake;

-Provide genuine unbiased counseling to students in terms of destinations, institutions and the program of studies.

-Respects towards students and their parents regardless of their backgrounds.

-The counselor must provide information to the students on all of the facets of the programs, lifestyles, costs and all associated current and relevant information.

-Counselor must provide a customer survey form after their counseling sessions.

-Counselors must provide the students the information on complaints procedures (available on the website) if requested.


Counselors must provide daily reports to the Area Manager on counseled students.


Accommodation in overall
We have fully assist our students with the accommodation and provide students variety accommodation option to meet their budget and lifestyle. Pre-departure

We have fully assist our students with the accommodation and provide students variety accommodation option to meet their budget and lifestyle.

Pre-departure
Pre-departure instruction and overseas services, arrange overseas accommodation, airport pick-up services, and help students to get familiar with registration procedures and so on.',
                    'is_published'          =>    1,
                ],
                [ 
                    'title' => 'Study abroad',
                    'content'=>'The advantages of overseas education are very high in numbers nowadays, since it facilitates better exposures in wider horizons. Obtaining a globally accepted degree from abroad allows people to travel and live anywhere in the world with highflying careers, a truly cosmopolitan experience.


Education and Visa Service (Edvise) provides overall support from pre-application to admission processes. Edvise offers a time-tested and cost effective service package for students to ensure successful admissions.


Edvise has long-standing experiences in connecting aspiring students with educational institutions across the globe. Today, we can place students in any country from Australia to France for any programs from Astronomy to Zen Buddhism. Distances are no more barriers for aspiring students as we lead a global life through Edvise.


Admissions Information
Edvise counselors help students in admission process by advising them of required documentations and assisting them with the necessary application forms and evidences. Edvise counselors work closely with admission staff at oversea institutions and help resolve unusual queries for our students. We also help students in gaining credits for their prior equivalent educational qualifications. Students applying through Edvise can also avail application fee waivers and get spot admission offers*.

General Admission Requirement
Documentation you’ll need to apply:


-A completed and signed school or university application form

-A certified copy of your academic results, including full academic transcripts

-A certified copy of your passport’s personal details page

-TOEFL score taken within the last 24 months with an overall Score of 525 / 197 / 70 and above with the minimum validity of 6 months at the time of applying. OR

-IELTS score taken within the last 24 months with an overall score of 5.5/ 6.0, with the minimum validity of 6 months at the time of applying. If the student doesn’t satisfy the above criteria for English, he will need to study ESL.

-Personal Statement (Essay)

-Letter of Recommendation: Students are required to submit at least one recommendation letter, preferably from a faculty member under who has taught the student in college / school.

-If original documents aren’t in English, they’ll need to supply certified translations as well as the documents.

Counselors
Edvise counselors are trained in counseling students for multiple destinations and at all levels of education. Edvise is one of the few agencies that sends its counselors overseas to be trained within institutions on programs, student experiences and lifestyles on campuses.


A large number of our counselors and managers have teaching backgrounds and therefore, have an in-depth understanding of student needs and career aspirations. A number of our counselors and their family members have been educated overseas which helps them guide students through their personal experiences. Edvise counselors have expertise to offer you Admission counseling in - school level programs, diploma, undergraduate, postgraduate and doctoral level programs.

Our counselors will advise you on Visa requirements and help you in every step till you get your visa.


The following are the counseling protocols within E that every counselor must undertake;

-Provide genuine unbiased counseling to students in terms of destinations, institutions and the program of studies.

-Respects towards students and their parents regardless of their backgrounds.

-The counselor must provide information to the students on all of the facets of the programs, lifestyles, costs and all associated current and relevant information.

-Counselor must provide a customer survey form after their counseling sessions.

-Counselors must provide the students the information on complaints procedures (available on the website) if requested.


Counselors must provide daily reports to the Area Manager on counseled students.


Accommodation in overall
We have fully assist our students with the accommodation and provide students variety accommodation option to meet their budget and lifestyle. Pre-departure

We have fully assist our students with the accommodation and provide students variety accommodation option to meet their budget and lifestyle.

Pre-departure
Pre-departure instruction and overseas services, arrange overseas accommodation, airport pick-up services, and help students to get familiar with registration procedures and so on.',
                    'is_published'          =>    1,
                ],
                [ 
                    'title' => 'Study abroad',
                    'content'=>'The advantages of overseas education are very high in numbers nowadays, since it facilitates better exposures in wider horizons. Obtaining a globally accepted degree from abroad allows people to travel and live anywhere in the world with highflying careers, a truly cosmopolitan experience.


Education and Visa Service (Edvise) provides overall support from pre-application to admission processes. Edvise offers a time-tested and cost effective service package for students to ensure successful admissions.


Edvise has long-standing experiences in connecting aspiring students with educational institutions across the globe. Today, we can place students in any country from Australia to France for any programs from Astronomy to Zen Buddhism. Distances are no more barriers for aspiring students as we lead a global life through Edvise.


Admissions Information
Edvise counselors help students in admission process by advising them of required documentations and assisting them with the necessary application forms and evidences. Edvise counselors work closely with admission staff at oversea institutions and help resolve unusual queries for our students. We also help students in gaining credits for their prior equivalent educational qualifications. Students applying through Edvise can also avail application fee waivers and get spot admission offers*.

General Admission Requirement
Documentation you’ll need to apply:


-A completed and signed school or university application form

-A certified copy of your academic results, including full academic transcripts

-A certified copy of your passport’s personal details page

-TOEFL score taken within the last 24 months with an overall Score of 525 / 197 / 70 and above with the minimum validity of 6 months at the time of applying. OR

-IELTS score taken within the last 24 months with an overall score of 5.5/ 6.0, with the minimum validity of 6 months at the time of applying. If the student doesn’t satisfy the above criteria for English, he will need to study ESL.

-Personal Statement (Essay)

-Letter of Recommendation: Students are required to submit at least one recommendation letter, preferably from a faculty member under who has taught the student in college / school.

-If original documents aren’t in English, they’ll need to supply certified translations as well as the documents.

Counselors
Edvise counselors are trained in counseling students for multiple destinations and at all levels of education. Edvise is one of the few agencies that sends its counselors overseas to be trained within institutions on programs, student experiences and lifestyles on campuses.


A large number of our counselors and managers have teaching backgrounds and therefore, have an in-depth understanding of student needs and career aspirations. A number of our counselors and their family members have been educated overseas which helps them guide students through their personal experiences. Edvise counselors have expertise to offer you Admission counseling in - school level programs, diploma, undergraduate, postgraduate and doctoral level programs.

Our counselors will advise you on Visa requirements and help you in every step till you get your visa.


The following are the counseling protocols within E that every counselor must undertake;

-Provide genuine unbiased counseling to students in terms of destinations, institutions and the program of studies.

-Respects towards students and their parents regardless of their backgrounds.

-The counselor must provide information to the students on all of the facets of the programs, lifestyles, costs and all associated current and relevant information.

-Counselor must provide a customer survey form after their counseling sessions.

-Counselors must provide the students the information on complaints procedures (available on the website) if requested.


Counselors must provide daily reports to the Area Manager on counseled students.


Accommodation in overall
We have fully assist our students with the accommodation and provide students variety accommodation option to meet their budget and lifestyle. Pre-departure

We have fully assist our students with the accommodation and provide students variety accommodation option to meet their budget and lifestyle.

Pre-departure
Pre-departure instruction and overseas services, arrange overseas accommodation, airport pick-up services, and help students to get familiar with registration procedures and so on.',
                    'is_published'          =>    1,
                ],
                [ 
                    'title' => 'Study abroad',
                    'content'=>'The advantages of overseas education are very high in numbers nowadays, since it facilitates better exposures in wider horizons. Obtaining a globally accepted degree from abroad allows people to travel and live anywhere in the world with highflying careers, a truly cosmopolitan experience.


Education and Visa Service (Edvise) provides overall support from pre-application to admission processes. Edvise offers a time-tested and cost effective service package for students to ensure successful admissions.


Edvise has long-standing experiences in connecting aspiring students with educational institutions across the globe. Today, we can place students in any country from Australia to France for any programs from Astronomy to Zen Buddhism. Distances are no more barriers for aspiring students as we lead a global life through Edvise.


Admissions Information
Edvise counselors help students in admission process by advising them of required documentations and assisting them with the necessary application forms and evidences. Edvise counselors work closely with admission staff at oversea institutions and help resolve unusual queries for our students. We also help students in gaining credits for their prior equivalent educational qualifications. Students applying through Edvise can also avail application fee waivers and get spot admission offers*.

General Admission Requirement
Documentation you’ll need to apply:


-A completed and signed school or university application form

-A certified copy of your academic results, including full academic transcripts

-A certified copy of your passport’s personal details page

-TOEFL score taken within the last 24 months with an overall Score of 525 / 197 / 70 and above with the minimum validity of 6 months at the time of applying. OR

-IELTS score taken within the last 24 months with an overall score of 5.5/ 6.0, with the minimum validity of 6 months at the time of applying. If the student doesn’t satisfy the above criteria for English, he will need to study ESL.

-Personal Statement (Essay)

-Letter of Recommendation: Students are required to submit at least one recommendation letter, preferably from a faculty member under who has taught the student in college / school.

-If original documents aren’t in English, they’ll need to supply certified translations as well as the documents.

Counselors
Edvise counselors are trained in counseling students for multiple destinations and at all levels of education. Edvise is one of the few agencies that sends its counselors overseas to be trained within institutions on programs, student experiences and lifestyles on campuses.


A large number of our counselors and managers have teaching backgrounds and therefore, have an in-depth understanding of student needs and career aspirations. A number of our counselors and their family members have been educated overseas which helps them guide students through their personal experiences. Edvise counselors have expertise to offer you Admission counseling in - school level programs, diploma, undergraduate, postgraduate and doctoral level programs.

Our counselors will advise you on Visa requirements and help you in every step till you get your visa.


The following are the counseling protocols within E that every counselor must undertake;

-Provide genuine unbiased counseling to students in terms of destinations, institutions and the program of studies.

-Respects towards students and their parents regardless of their backgrounds.

-The counselor must provide information to the students on all of the facets of the programs, lifestyles, costs and all associated current and relevant information.

-Counselor must provide a customer survey form after their counseling sessions.

-Counselors must provide the students the information on complaints procedures (available on the website) if requested.


Counselors must provide daily reports to the Area Manager on counseled students.


Accommodation in overall
We have fully assist our students with the accommodation and provide students variety accommodation option to meet their budget and lifestyle. Pre-departure

We have fully assist our students with the accommodation and provide students variety accommodation option to meet their budget and lifestyle.

Pre-departure
Pre-departure instruction and overseas services, arrange overseas accommodation, airport pick-up services, and help students to get familiar with registration procedures and so on.',
                    'is_published'          =>    1,
                ],
               

            ]);

	}
}
