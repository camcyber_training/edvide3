<?php

use Illuminate\Database\Seeder;


class TypeTableSeeder extends Seeder
{
  
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    
	{
	     DB::table('types')->insert(
            [
                
                [ 
                    'title' => 'Family Based Visa (Spouses and Family members) ',
                ],
                [ 
                    'title' => 'Fiancé Visa',
                ],

                [ 
                    'title' => 'Visitor Visa (Tourist Visa)',
                    
                ],

                [ 
                    'title' => 'Employment Visa',
                ],

                [ 
                    'title' => 'Work Visa',
                ],
                
                [ 
                    'title' => 'Investment Visa',
                ],

                [
                    'title' => 'Investor Visa',
                ]

            ]);

	}
}
