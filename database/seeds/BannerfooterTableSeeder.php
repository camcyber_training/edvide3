<?php

use Illuminate\Database\Seeder;


class BannerfooterTableSeeder extends Seeder
{
  
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    
	{
	     DB::table('bannerfooter')->insert(
            [
                
                [ 
                    'title' => 'Have any Question?',
                    'contents' => 'We are here to help. Send us an email or call us at +855 16-949-294. Please feel free to contact our expert.',
                    'image' => "public/frontend/assets/image/plx-cta.jpg",
                    'is_published'          =>    1,
                ],
               

            ]);

	}
}
