<?php

use Illuminate\Database\Seeder;

class ManagementTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('management')->insert(
            [
                [
                    'title'                 => '- Managing Director',
                    'name'                  => 'Kunthel OUM',
                    'contents'              => 'Kunthel is passionate in social services and he is a young social entrepreneur with extensive experience in visa consultancy, education advisory, and new started-up in digital biz, education and hospitality. He has started his consultancy service since 2003 and officially registered as Edvise. Prior to this, he had experiences in market analysis, feasibility studies, due diligent and investment analysis.',
                    'image'                 => 'public/frontend/assets/image/team/team-a.jpg',
                    'is_published'          =>    1,
                ],

                [
                    'title'                 => '- Education Dept Manager',
                    'name'                  => 'Dolla PHA',
                    'contents'              => 'In regards to matters related to studying abroad and documents preparations, Dolla has been a responsible consultant for those particular information. He has years of experiences in these fields since he was also once a Managing assistant of the Student Recruitment Department. His skills in translation is professional due to his past involvements with English teaching for more than 10 years. He has a Master degree in Business Administration and has been to undeniably various and useful workshops.',
                    'image'                 => 'public/frontend/assets/image/team/team-b.jpg',
                    'is_published'          =>    1,
                ],

                [
                    'title'                 => '- Visa Service Dept Manager',
                    'name'                  => 'Eiza CHHUN',
                    'contents'              => 'Eiza is managing the whole visa department that strongly influences both Cambodians and foreigners as he assists in the documents in regards to visa applications since 2016. His work experiences started in 2010 when he began working as a senior consultant and a translator. Furthermore, he has experiences in training the staff and he is punctual on meetings. He had earned his Master’s degree in Business Administration in 2018. He was rewarded with a scholarship and had earned a degree in English Literature in 2013.',
                    'image'                 => 'public/frontend/assets/image/team/team-c.jpg',
                    'is_published'          =>    1,
                ],

                [
                    'title'                 => '- Education Edviser',
                    'name'                  => 'Molyka Brasoeur',
                    'contents'              => 'Kunthel is passionate in social services and he is a young social entrepreneur with extensive experience in visa consultancy, education advisory, and new started-up in digital biz, education and hospitality. He has started his consultancy service since 2003 and officially registered as Edvise. Prior to this, he had experiences in market analysis, feasibility studies, due diligent and investment analysis. He has jointed private equity firm as a business consultant for 3 years. ',
                    'image'                 => 'public/frontend/assets/image/team/team-d.jpg',
                    'is_published'          =>    1,
                ],
                
               


                
            ]
        );
    }
}
