<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
   
    protected $table = 'activity';
   
    public function activityImage(){

        return $this->hasMany('App\Model\ActivityImage','activity_id');//1->many
    }
}
