<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class ServiceSpecific extends Model
{
   
    protected $table = 'service_specific'; 

    public function service(){
        return $this->belongsTo('App\Model\Service','service_id');
    } 
    public function type(){
        return $this->belongsTo('App\Model\Type','type_id');
    } 

    public function faqs(){
        return $this->hasMany('App\Model\ServiceSpecificFaq', 'specific_id');
    }
}
