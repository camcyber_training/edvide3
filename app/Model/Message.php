<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
   
    protected $table = 'message';

    public function careers(){
        return $this->belongsTo('App\Model\Careers');
    }
    
}
