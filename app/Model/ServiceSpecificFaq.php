<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class ServiceSpecificFaq extends Model
{
    protected $table = 'service_specific_faqs'; 

    public function specific(){
        return $this->belongsTo('App\Model\ServiceSpecific','service_id');
    } 
   
}
