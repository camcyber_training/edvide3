<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
   
    protected $table = 'types'; 

    public function services(){
        return $this->hasMany('App\Model\Service','service_id');//1->many
    }


   
}
