<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class ActivityImage extends Model
{
   
    protected $table = 'activity_image'; 

    public function activity(){
        return $this->belongsTo('App\Model\Activity','activity_id');
    } 
   
}
