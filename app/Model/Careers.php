<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Careers extends Model
{
   
    protected $table = 'careers';
   
    public function message() {
        return $this->hasMany('App\Model\Message');
    }
   
}
