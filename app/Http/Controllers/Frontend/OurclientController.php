<?php

namespace App\Http\Controllers\Frontend;


use App\Http\Controllers\Frontend\FrontendController;


use App\Model\Partner as Partner;
use App\Model\Bannerfooter as Bannerfooter;
use App\Model\Ourclient as Ourclient;


class OurclientController extends FrontendController
{
    
   
    public function index() {
    	$defaultData = $this->defaultData();
    	$partner = Partner::select('id','image','url','is_published')->where('is_published',1)->get();
    	$ourclient = ourclient::select('id','image','contents','name','title','is_published')->where('is_published',1)->get();
		$bannerfooter = Bannerfooter::select('id','image','contents','title','is_published')->where('is_published',1)->get();
	
        return view ('frontend.about.our-client',['defaultData'=>$defaultData,'partner'=>$partner, 'bannerfooter' => $bannerfooter,'ourclient'=> $ourclient,]);

    }
}
