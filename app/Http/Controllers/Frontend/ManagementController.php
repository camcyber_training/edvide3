<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\FrontendController;

use App\Model\Partner as Partner;
use App\Model\Management as Management;
use App\Model\Bannerfooter as Bannerfooter;


class ManagementController extends FrontendController
{
    
    public function index() {
		$defaultData = $this->defaultData();
    	$partner = Partner::select('id','image','url','is_published')->where('is_published',1)->get();
    	$management = Management::select('id','image','contents','title','name','is_published')->where('is_published',1)->get();
    	$bannerfooter = Bannerfooter::select('id','image','contents','title','is_published')->where('is_published',1)->get();

        return view ('frontend.about.management',['defaultData'=>$defaultData, 'partner'=>$partner,'management'=> $management, 'bannerfooter' => $bannerfooter,]);

    }
}
