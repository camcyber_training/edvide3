<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Frontend\FrontendController;


use App\Model\Slide as Slide;
use App\Model\Whatwedo as Whatwedo;
use App\Model\Constulting as Constulting;
use App\Model\Ourmanagement as Ourmanagement;
use App\Model\Ourupdate as Ourupdate;
use App\Model\Partner as Partner;
use App\Model\Whoweare as Whoweare;
use App\Model\Why as Why;
use App\Model\Bannerfooter as Bannerfooter;



class HomeController extends FrontendController
{
    
   
    public function index() {

		$defaultData = $this->defaultData();
		
		$slides = Slide::select('id','name','title','url','image','is_published')->where('is_published',1)->get();
		$whoweare = Whoweare::select('id','image','contents','title')->first();
		$whatwedo = Whatwedo::select('id','image','title','name','is_published')->where('is_published',1)->get();
		$why = Why::select('id','title','image','name','is_published')->where('is_published',1)->get();
    	$partner = Partner::select('id','image','url','is_published')->where('is_published',1)->get();
    	$constulting = Constulting::select('id','image','description','contents','title','is_published')->where('is_published',1)->get();
    	$ourmanagement = Ourmanagement::select('id','image','contents','title','is_published')->where('is_published',1)->get();
    	$ourupdate= Ourupdate::select('id','image','title','description','contents','created_at','is_published')->where('is_published',1)->get();
		$bannerfooter = Bannerfooter::select('id','image','contents','title','is_published')->where('is_published',1)->get();
		

        return view ('frontend.home',['defaultData'=>$defaultData,'slide'=>$slides,'why'=>$why,'whatwedo'=>$whatwedo,'constulting'=>$constulting,'ourmanagement'=>$ourmanagement,'ourupdate'=>$ourupdate,'partner' =>$partner,'whoweare' =>$whoweare,'bannerfooter' => $bannerfooter,]);

	}
	
	

}