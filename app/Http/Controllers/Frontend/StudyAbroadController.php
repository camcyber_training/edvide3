<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use App\Model\ServiceSpecific as ServiceSpecific;
use App\Model\Service as Service;
use App\Model\Partner as Partner;



class StudyAbroadController extends FrontendController
{
    
   
    public function index() {
            
    	$defaultData = $this->defaultData();
        $partner = Partner::select('id','image','contents','title','is_published')->where('is_published',1)->get(); 
        $service = Service::select('id','content','title')->first(); 
        $serviceSpecific = ServiceSpecific::select('id','content','title','is_published')->where('is_published',1)->get(); 	
        return view ('frontend.services.study-abroad',['defaultData'=>$defaultData,'partner'=>$partner,'service'=>$service,'serviceSpecific'=>$serviceSpecific]);

    }
}
