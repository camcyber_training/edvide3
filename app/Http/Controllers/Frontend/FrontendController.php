<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;


use App\Model\Aboutbanner as Aboutbanner;
use App\Model\Partner as Partner;
use App\Model\Service;

class FrontendController extends Controller
{
  	
	public $defaultData = array();
    public function __construct(){
      
    }

    public function defaultData($locale = "en"){


    	App::setLocale($locale);

        //Current Language
        $parameters = Route::getCurrentRoute()->parameters();
        
        $this->defaultData['aboutbanner'] =  Aboutbanner::select('id','image')->get();
        $this->defaultData['services']    = Service::select('id', 'title', 'image', 'slug')->where('is_published', 1)->get();
        return $this->defaultData;
    }
    
}