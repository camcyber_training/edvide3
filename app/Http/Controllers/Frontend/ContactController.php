<?php

namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;

use Session;
use Validator;
// use Telegram\Bot\Laravel\Facades\Telegram;

use App\Model\Massage as Message;

class ContactController extends FrontendController
{
    
    public function index() {
        $defaultData = $this->defaultData();
        return view('frontend.contact', ['defaultData'=>$defaultData,]);
    }

    public function sendMassage(Request $request){
        
        $now        = date('Y-m-d H:i:s');
        
        $data = array(
            'name'              =>      $request->input('name'),
            'phone'             =>      $request->input('phone'),
            'email'             =>      $request->input('email'),
            'company'           =>      $request->input('company'),
            'message'           =>      $request->input('message'),
            'created_at'        =>      $now
        );
        
        Session::flash('invalidData', $data );
        Validator::make(
            $request->all(), 
            [
                'name' => 'required',
                'phone' => 'required',
                'email' => 'required',
                'message' => 'required',
                'company' => 'required',
            ], 

            [
                
            ])->validate();

        
        $id = Message::insertGetId($data);
        
        $request->session()->flash('msg', 'Your request has been sent! We will respone you soon.' );
        return redirect()->back();
    }
}
