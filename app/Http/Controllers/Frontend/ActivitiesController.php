<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;

use App\Model\Activity as Activity;


class ActivitiesController extends FrontendController
{
    
    public function index() {
        $defaultData = $this->defaultData(); 
        $activity= Activity::select('id','image','title','contents','is_published')->with('activityImage')->where('is_published',1)->get();
    
        return view('frontend.activities', ['defaultData'=>$defaultData,'activity'=>$activity,]);

    }
}

