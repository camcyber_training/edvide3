<?php

namespace App\Http\Controllers\Frontend;


use App\Http\Controllers\Frontend\FrontendController;


use App\Model\Ourupdate as Ourupdate;
use App\Model\Aboutbanner as Aboutbanner;
use App\Model\Constulting as Constulting;

class ConstultDetailController extends FrontendController
{
    
   
    public function index($id) {
    	$defaultData = $this->defaultData();
		$constulting = Constulting::select('id','image','description','contents','title')->get();
		$detail		 = Constulting::select('id','image','title','description','contents','created_at')->where('id',$id)->first();
        return view ('frontend.constult-detail',['defaultData'=>$defaultData,'detail'=>$detail,'constulting'=>$constulting,]); 
  
    }

}