<?php

namespace App\Http\Controllers\Frontend;


use App\Http\Controllers\Frontend\FrontendController;

use App\Model\ServiceSpecific as ServiceSpecific;


class StudyAbroaddetailController extends FrontendController
{
    
   
    public function index($id) {

        $defaultData = $this->defaultData();

        $serviceSpecific = ServiceSpecific::select('id','content','title','service_id')->first(); 
        return view ('frontend.services.study-abroad-detail',['defaultData'=>$defaultData,'serviceSpecific'=>$serviceSpecific,]);

    }

}
