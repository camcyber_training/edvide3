<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use App\Model\ServiceSpecific as ServiceSpecific;
use App\Model\Service as Service;
use App\Model\Partner as Partner;
class ServiceController extends FrontendController
{
    
    public function index($slug = '') {
    	$defaultData = $this->defaultData();
        $data = Service::select('id','content','title', 'slug', 'image')->where('slug', $slug)->with('servicesSpecific')->first();
        $specifics = [];
        if($data){
            $specifics = $data->servicesSpecific()->get();
            
        }
        if($data){
            return view ('frontend.services.service',['defaultData'=>$defaultData,'data'=>$data, 'specifics'=>$specifics]);
        }else{
            return '404';
        } 
        
    }

    public function detail ($slug_service = '', $slug_specific = ''){
        $defaultData = $this->defaultData();
        $service = Service::select('id','content','title', 'slug', 'image')->where('slug', $slug_service)->with('servicesSpecific')->first();
        $specific = ServiceSpecific::select('id','content','title', 'slug', 'service_id', 'type_id')->where('slug', $slug_specific)->first();

        $specifics = [];
        if($service){
            $specifics = $service->servicesSpecific()->get();
            
        }
        
        $faqs = [];
        if($specific){
            $faqs = $specific->faqs()->get();
            
        } 

        return view ('frontend.services.detail',['defaultData'=>$defaultData,'data'=>$service, 'specific'=>$specific, 'specifics' => $specifics, 'faqs'=>$faqs]);
    }
}
