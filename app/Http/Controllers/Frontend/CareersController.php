<?php
namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Frontend\FrontendController;

use App\Model\Careers as Careers;

class CareersController extends FrontendController
{
    
 

    public function index() {

    		$defaultData = $this->defaultData();
            $careers= Careers::select('id','title','post','closing','description','contents','created_at','is_published')->where('is_published',1)->paginate(4);
            
        return view ('frontend.careers',['defaultData'=>$defaultData,'careers'=>$careers]);

    }
   
}

