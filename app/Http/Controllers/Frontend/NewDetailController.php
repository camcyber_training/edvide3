<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use App\Model\Ourupdate as Ourupdate;
use App\Model\Aboutbanner as Aboutbanner;


class NewDetailController extends FrontendController
{
    
   
    public function index($id) {
    	$defaultData = $this->defaultData();
		
		$ourupdate= Ourupdate::select('id','image','title','contents','created_at','is_published')->where('is_published',1)->get();
		$detail= Ourupdate::select('id','image','title','contents','created_at','is_published')->where('id',$id)->first();
        return view ('frontend.new-detail',['defaultData'=>$defaultData,'ourupdate'=>$ourupdate,'detail'=>$detail,]); 

    }

}