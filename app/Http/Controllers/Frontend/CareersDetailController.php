<?php
namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use Session;
use Validator;
use App\Model\Careers as Careers;
use App\Model\Message as Message;
class CareersDetailController extends FrontendController
{
    
   
    public function index($id) {
    	$defaultData = $this->defaultData();
		$detail		 = Careers::select('id','title','post','closing','description','contents','created_at')->where('id',$id)->first();
        return view ('frontend.careers-detail',['defaultData'=>$defaultData,'detail'=>$detail,]); 
  
    }
    public function sendMessage(Request $request){
      
        $now        = date('Y-m-d H:i:s');
        
        $data = array(
            'careers_id'     => $request->input('careers_id'),
            'name'           =>      $request->input('name'),
            'phone'          =>      $request->input('phone'),
            'email'          =>      $request->input('email'),
            
            'created_at'     =>      $now
        );
        $file = FileUpload::uploadFile($request, 'file', 'uploads/file');
        if($file != ""){
            $data['file'] = $file; 
        }
        Session::flash('invalidData', $data );
        Validator::make(
            $request->all(), 
            [
            
                'name' => 'required',
                'phone' => 'required',
                'email' => 'required',
            ], 
            [
                
            ])->validate();

        
        $id = Message::insertGetId($data);
        
        $request->session()->flash('msg', 'Your request has been sent! We will respone you soon.' );
        return redirect()->back();
    }
}