<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;

use App\Model\Partner as Partner;
use App\Model\Service as Service;


class BussinessController extends FrontendController
{
    
   
    public function index() {

    		$defaultData = $this->defaultData();
            $partner = Partner::select('id','image','contents','title','is_published')->where('is_published',1)->get();
            $service = Service::select('id','title')->with('servicesSpecific')->first();
            
        return view ('frontend.services.bussiness-solution',['defaultData'=>$defaultData,'partner'=>$partner,'service'=>$service]);

    }
}

