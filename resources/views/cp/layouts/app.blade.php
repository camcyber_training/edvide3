@extends('cp.layouts.master')

@section ('headercss')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <link rel="stylesheet" href="{{ asset ('public/cp/css/lib/font-awesome/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset ('public/cp/css/lib/bootstrap-sweetalert/sweetalert.css') }}">
    <link rel="stylesheet" href="{{ asset ('public/cp/css/lib/summernote/summernote.css') }}"/>
    <link rel="stylesheet" href="{{ asset ('public/cp/css/main.css') }}">
    <script type="text/javascript" src="{{ asset ('public/cp/js/lib/jquery/jquery.min.js') }}"></script>
    @yield('appheadercss')
@endsection

@section ('bodyclass')
    class="with-side-menu control-panel control-panel-compact"
@endsection

@section ('header')

<header class="site-header">
    <div class="container-fluid">
        <a target="_blank" href="{{ url('/') }}" class="site-logo">
            <img  class="hidden-md-down" src="{{ asset ('public/cp/img/logo.png') }}" alt="">
            <img class="hidden-lg-up" src="{{ asset ('public/cp/img/logo.png') }}" alt="">
        </a>
        <button class="hamburger hamburger--htla">
            <span>toggle menu</span>
        </button>
        <div class="site-header-content">
            <div class="site-header-content-in">
                <div class="site-header-shown">
                    
                    <div class="dropdown user-menu">
                        <button class="dropdown-toggle" id="dd-user-menu" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="{{ asset (Auth::user()->avatar) }}" alt="">
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-user-menu">
                            <a class="dropdown-item" href="{{ route('cp.user.profile.edit') }}"><span class="fa fa-user"></span> Profile</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('cp.auth.logout') }}"><span class="fa fa-sign-out"></span> Logout</a>
                        </div>
                    </div>

                    <button type="button" class="burger-right">
                        <i class="font-icon-menu-addl"></i>
                    </button>
                </div><!--.site-header-shown-->

                <div class="mobile-menu-right-overlay"></div>
                
            </div><!--site-header-content-in-->
        </div><!--.site-header-content-->
    </div><!--.container-fluid-->
</header><!--.site-header-->
@endsection

@section ('menu')
    @php ($menu = "")
    @if(isset($_GET['menu']))
        @php( $menu = $_GET['menu'])
    @endif
    

    <div class="mobile-menu-left-overlay"></div>
    <nav class="side-menu">
        <ul class="side-menu-list">   


                 <li class="@yield('active-main-menu-homepage') red with-sub">
                <span>
                    <i class=" font-icon fa fa-home"></i>
                    <span class="lbl">Home</span>
                </span>
                <ul>
                   
                    <li class=""> <a href="{{ route('cp.slide.index') }}"><span class="lbl">Slide</span></a></li>
                    <li class=""> <a href="{{ route('cp.whoweare.index') }}"><span class="lbl">Who We Are</span></a></li>
                    <li class=""> <a href="{{ route('cp.why.index') }}"><span class="lbl">Why choosing Edvise?</span></a></li>
                   <li class=""> <a href="{{ route('cp.ourmanagement.index') }}"><span class="lbl">Our Management</span></a></li>
                   <li class=""> <a href="{{ route('cp.partner.index') }}"><span class="lbl">Partner</span></a></li>
                   <li class=""> <a href="{{ route('cp.ourupdate.index')}}"><span class="lbl">Our Update</span></a></li>
                </ul>
            </li>
                
            <li class="red @yield('active-main-menu-banner')">
                    <a href="{{ route('cp.aboutbanner.index')}}">
                    <i class="fa fa-picture-o"></i>
                    <span>
                        <span class="lbl">Banners</span>
                    </span>
                    </a>
            </li>
             <li class="@yield('active-main-menu-about-us') red with-sub">
                <span>
                    <i class=" fa fa-info-circle"></i>
                    <span class="lbl">About Us</span>
                </span>
                <ul>
                    <li class=""> <a href="{{ route('cp.director.index') }}"><span class="lbl">Director</span></a></li>
                     <li class=""> <a href="{{ route('cp.management.index') }}"><span class="lbl">Management</span></a></li>     
                    <li class=""> <a href="{{ route('cp.ourclient.index')}}"><span class="lbl">Our Clients</span></a></li>                                          
                </ul>
            </li>

            <li class="red @yield('active-main-menu-service') red with-sub">
                <span>
                    <i class="fa fa-server"></i>
                    <span class="lbl">Service</span>
                </span>
                <ul>
                    <li class=""> <a href="{{ route('cp.service.index') }}"><span class="lbl">Main</span></a></li>
                    <li class=""> <a href="{{ route('cp.type.index') }}"><span class="lbl">Type</span></a></li>
                </ul>
            </li>
            <li class="red @yield('active-main-menu-carrers')">
                    <a href="{{ route('cp.careers.index') }}">
                    <i class="fa fa-credit-card-alt" aria-hidden="true"></i>
                    <span>
                        <span class="lbl">Careers</span>
                    </span>
                    </a>
            </li>
            <li class="red @yield('active-main-menu-activity')">
                    <a href="{{ route('cp.activity.index') }}">
                    <i class="fa fa-desktop"></i>
                    <span>
                        <span class="lbl">Activity</span>
                    </span>
                    </a>
            </li>
            <!-- <li class="red @yield('active-main-menu-message')">
                <li class="red @yield('active-main-menu-message')">
                    <a href="{{ route('cp.message.index') }}">
                    <span>
                        <i class=" fa fa-envelope"></i>
                        <span class="lbl">Job Announcement</span>
                    </span>
                    </a>
                </li>
            </li> -->
            <li class="red @yield('active-main-menu-massage')">
                <li class="red @yield('active-main-menu-massage')">
                    <a href="{{ route('cp.massage.index') }}">
                    <span>
                        <i class=" font-icon fa fa-comment"></i>
                        <span class="lbl">Message</span>
                    </span>
                    </a>
                </li>
            </li>
       
            @if(Auth::user()->position_id == 1)
             <li class="red @yield('active-main-menu-user')">
                <a href="{{ route('cp.user.user.index') }}">
                <span>
                    <i class="fa fa-users"></i>
                    <span class="lbl">Users</span>
                </span>
                </a>
            </li>
            @endif

           
        </ul>
    </nav><!--.side-menu-->

@endsection

@section ('content')
    <div class="page-content">
        
        @yield ('page-content')
        
    </div>
@endsection




@section ('bottomjs')
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
        @yield ('imageuploadjs')
        <script type="text/javascript" src="{{ asset ('public/cp/js/lib/tether/tether.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset ('public/cp/js/lib/bootstrap/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset ('public/cp/js/plugins.js') }}"></script>
        <script type="text/javascript" src="{{ asset ('public/cp/js/lib/lobipanel/lobipanel.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset ('public/cp/js/lib/match-height/jquery.matchHeight.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset ('public/cp/js/lib/bootstrap-sweetalert/sweetalert.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset ('public/cp/js/lib/blockUI/jquery.blockUI.js') }}"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        <script src="{{ asset ('public/cp/js/lib/bootstrap-select/bootstrap-select.min.js')}}"></script>
        <script src="{{ asset ('public/cp/js/lib/select2/select2.full.min.js')}}"></script>
       <script type="text/javascript" src="{{ asset ('public/cp/js/lib/summernote/summernote.min.js') }}"></script>
        <script src="{{ asset ('public/cp/js/app.js') }}"></script>
        <script src="{{ asset ('public/cp/js/camcyber.js') }}"></script>
        @yield('appbottomjs')

        @if(Session::has('msg'))
        <script type="text/JavaScript">
            toastr.success("{!!Session::get('msg')!!}");
        </script>
        @endif
        @if(Session::has('error'))
        <script type="text/JavaScript">
            toastr.error("{!!Session::get('error')!!}");
        </script>
        @endif
@endsection