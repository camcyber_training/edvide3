@extends('cp.service.specific.tab')
@section ('section-title', 'Edit Faqs')
@section ('tab-active-specific', 'active')
@section('about-active-faqs', 'active')
@section ('section-css')
	<link href="{{ asset ('public/cp/css/plugin/fileinput/fileinput.min.css') }}" media="all" rel="stylesheet" type="text/css"/>
    <link href="{{ asset ('public/cp/css/plugin/fileinput/theme.css') }}" media="all" rel="stylesheet" type="text/css"/>
	<!-- some CSS styling changes and overrides -->
	<style>
		.kv-avatar .file-preview-frame,.kv-avatar .file-preview-frame:hover {
		    margin: 0;
		    padding: 0;
		    border: none;
		    box-shadow: none;
		    text-align: center;
		}
		.kv-avatar .file-input {
		    display: table-cell;
		    max-width: 220px;
		}
	</style>
@endsection

@section ('imageuploadjs')
    <script type="text/javascript" src="{{ asset ('public/cp/js/plugin/fileinput/fileinput.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset ('public/cp/js/plugin/fileinput/theme.js') }}" type="text/javascript"></script>
@endsection

@section ('section-js')
	
@endsection

@section ('tap-content')
	@include('cp.layouts.error')
	<form id="form" action="{{ route('cp.service.update-faqs', ['id'=>$id, 'specific_id'=>$specific_id, 'faq_id'=>$faq_id]) }}" name="form" method="POST"  enctype="multipart/form-data">
		{{ csrf_field() }}
		{{ method_field('POST') }}

		
		<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="title">Question</label>
				<div class="col-sm-10">
					<input 	id="title"
							name="title"
						   	value = "{{$data->title}}"
						   	type="text"
						   	placeholder = "Enter title."
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />
							
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="content">Answer</label>
				<div class="col-sm-10">
					<div class="summernote-theme-1">
						<textarea  id="content" name="content" class="form-control summernote"> {{$data->content}} </textarea>
					</div>	
				</div>
			</div>

			<div class="form-group row">
					<label class="col-sm-2 form-control-label" for="kh_content">Published</label>
					<div class="col-sm-10">
						<div class="checkbox-toggle">
							<input id="status-status" name="status" type="checkbox"  @if($data->is_published ==1 ) checked @endif >
							<label onclick="booleanForm('status')" for="status-status"></label>
						</div>
					</div>
			</div>

		
		<div class="form-group row">
			<label class="col-sm-2 form-control-label"></label>
			<div class="col-sm-10">
				<button type="submit" class="btn btn-success"> <fa class="fa fa-cog"></i> Update</button>
				<button type="button" onclick="deleteConfirm('{{ route($route.'.trash-faqs', ['id'=>$id, 'specific_id'=>$specific_id, 'faq_id'=>$data->id]) }}', '{{ route('cp.service.faqs', ['id'=>$id,'specific_id'=>$specific_id]) }}')" class="btn btn-danger"> <fa class="fa fa-trash"></i> Delete</button>
			</div>
		</div>
	</form>
@endsection