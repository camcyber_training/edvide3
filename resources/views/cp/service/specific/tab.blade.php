@extends('cp.service.tab')
@section ('section-title', 'Edit Specific')
@section ('tab-active-specific', 'active')

@section ('tab-content')
	
	<section class="box-typical files-manager">
		<nav class="files-manager-side" style="height: auto;">
			<ul class="files-manager-side-list">
				<li><a href="{{ route('cp.service.edit-specific', ['id'=>$id, 'specific_id'=>$specific_id]) }}" class="@yield ('about-active-overview')">Overview</a></li>
				<li><a href="{{ route('cp.service.faqs', ['id'=>$id, 'specific_id'=>$specific_id]) }}" class="@yield ('about-active-faqs')">Faqs</a></li>
			</ul>
		</nav>

		<div class="files-manager-panel">
			<div class="files-manager-panel-in">
				<div class="container-fluid">
					@yield ('tap-content')
				</div>
			</div><!--.files-manager-panel-in-->
		</div><!--.files-manager-panels-->
	</section>
	
@endsection