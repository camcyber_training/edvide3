<!-- End Header -->	@extends('frontend.layouts.master')

@section('title', 'Welcome to Edvise')
@section('active-services', 'active')


@section ('content')
	
	<div class="banner banner-static">
		<div class="banner-cpn">
			<div class="container">
				<div class="content row">
					<div class="banner-text">						
					</div>
				</div>
			</div>
		</div>
		<div class="banner-bg imagebg">
			<img src=" {{asset (getAboutbanner('Service'))}} " alt="" />
		</div>
	</div>
	<div class="section section-contents section-pad">
		<div class="container">
			<div class="content row">
				<div class="row">
					<div class="col-md-8"style="margin-bottom: 40px;">
						<h2 class="heading-lg">{{ $data->title ?? '' }}</h2>
						<p class="">{!! $data->content ?? '' !!}</p>
						
					</div>
					
					<div class="col-md-4">
						<div class="sidebar-right">

							<div class="wgs-box wgs-menus">
								<div class="wgs-content">
									<ul class="list list-grouped">
										<li class="list-heading">
											<a href="{{ route('services', $data->slug) }}">
												<span>Main Services</span>
											</a>
											<ul>
												@foreach ( $specifics as $row )												
													<li><a href="{{ route('services-detail',['slug'=>$data->slug, 'specific'=> $row->slug]) }}">
														
														{{ $row->title }} 
													</a></li>
												@endforeach 
												
											</ul>
										</li>
									</ul>									
								</div>
							</div>				
						</div>
					</div>

				</div>	
			</div>
		</div>		
	</div>
@endsection