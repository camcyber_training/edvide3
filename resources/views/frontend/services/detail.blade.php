<!-- End Header -->	@extends('frontend.layouts.master')

@section('title', 'Welcome to Edvise')
@section('active-services', 'active')


@section ('content')

	@php($param_url = request()->segments()['2'])

	<div class="banner banner-static">
		<div class="banner-cpn">
			<div class="container">
				<div class="content row">
					<div class="banner-text">						
					</div>
				</div>
			</div>
		</div>
		<div class="banner-bg imagebg">
			<img src=" {{asset (getAboutbanner('Service'))}} " alt="" />
		</div>
	</div>
		<!-- #end Banner/Slider -->
	<div class="section section-contents section-pad">
		<div class="container">
			<div class="content row">
				<div class="row">
					<div class="col-md-8"style="margin-bottom: 40px;">
						<h2 class="heading-lg">{{ $specific->title ?? '' }}</h2>
						<p class="">{!! $specific->content ?? '' !!}</p>
						
						@if(sizeof($faqs) > 0)
							<h3 class="color-primary">FAQ</h3>
							<div class="panel-group accordion" id="general" role="tablist" aria-multiselectable="true">
								<!-- each panel -->
								@foreach($faqs as $row)
								<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="ques-i{{$row->id}}">
										<h4 class="panel-title">
											<a class="collapsed" role="button" data-toggle="collapse" data-parent="#general" href="#ques-ans-i{{$row->id}}" aria-expanded="false">
												{{$row->title}}
												<span class="plus-minus"><span></span></span>
											</a>
										</h4>
									</div>
									<div id="ques-ans-i{{$row->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="ques-i{{$row->id}}">
										<div class="panel-body">
											<p>{!!$row->content!!}</p>
										</div>
									</div>
								</div> 
								@endforeach
							</div>
						@endif

					</div>
					
					<div class="col-md-4">
						<div class="sidebar-right">
							<div class="wgs-box wgs-menus">
								<div class="wgs-content">
									<ul class="list list-grouped">
										<li class="list-heading">
											<a href="{{ route('services', $data->slug) }}">
												<span>Main Services</span>
											</a>
											<ul>
												@foreach ( $specifics as $row )												
													<li class="@if($param_url == $row->slug) active @endif">
														<a href="{{ route('services-detail',['slug'=>$data->slug, 'specific'=> $row->slug]) }}">
															{{ $row->title }} 
														</a>
													</li>
												@endforeach 
												
											</ul>
										</li>
									</ul>									
								</div>
							</div>					
						</div>
					</div>

				</div>	
			</div>
		</div>		
	</div>
@endsection