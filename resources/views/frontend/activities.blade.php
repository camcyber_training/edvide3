<!-- Content Section -->
<!-- Banner/Slider -->
	@extends('frontend.layouts.master')

@section('title', 'Welcome to Edvise')
@section('active-activities', 'current')


@section ('content')
	
<div class="banner banner-static">
			<div class="banner-cpn">
				<div class="container">
					<div class="content row">
						<div class="banner-text">						
						</div>
					</div>
				</div>
			</div>
			<div class="banner-bg imagebg">
				<img src=" {{asset (getAboutbanner('Activites'))}} " alt="" />
			</div>
</div>
	<div class="section section-contents section-pad">
		<div class="container">
			<div class="content row">
			@foreach($activity as $row)
				<div class="wide-md text-center">		
				</div>
				<div class="gallery gallery-col3 gallery-filled gallery-lightbox hover-fade">
					<ul class="gallery-list">
					@foreach($row->activityImage as $item)
						<li>
							<a href="{{ asset($item->image) }}">
								<div class="gallery-item"><img src="{{ asset($item->image) }}" alt="@by Author" title="Name of Photo"></div>
							</a>
						</li>
						@endforeach
					</ul>
				</div>
				@endforeach
			</div>
		</div>		
	</div>
	<!-- End Section -->
	@endsection