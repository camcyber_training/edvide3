@extends('frontend.layouts.master')

@section('title', 'Contact Us | '.env('APP_NAME'))
@section('active-contact', 'current')

@section ('content')

@section ('appbottomjs')
<script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

<script type="text/javascript">
    $(document).ready(function() {
        $("#submit-contact").submit(function(event) {
            submit(event);
        })

        @if(Session::has('invalidData'))
        $("#form").show();
        @endif

    })
    @if(Session::has('msg'))
    toastr.success("{{ Session::get('msg') }}");
    @endif

    function submit(event) {

        name = $("#name").val();
        company = $("#company").val();
        email = $("#email").val();
        message = $("#message").val();
        recaptcha = $('#g-recaptcha-response').val();

        if (name != 0) {
            if (validateEmail(email)) {
                if (company) {

                    if (recaptcha != "") {

                            @if(Session::has('msg'))
                            toastr.error("Please check robot verification");
                            @endif
                        } else {
                            toastr.error("Please check robot verification");
                            event.preventDefault();
                            $("#g-recaptcha-response").focus();
                        }
                    } else {
                        toastr.error("Please Select company");
                        event.preventDefault();
                        $("#company").focus();
                    }
                } else {
                        toastr.error("Please give us correct email");
                        event.preventDefault();
                        $("#email").focus();
                }
            }
        } else {
            toastr.error("Please enter your name");
            event.preventDefault();
            $("#name").focus();
        }
    }

    function showApplicationForm() {
        form = $("#form");
        if (form.is(":visible")) {
            form.hide();
        } else {
            form.show();
        }
    }
    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    function validatePhone(phone) {
        return phone.match(/(^[00-9].{8}$)|(^[00-9].{9}$)/);
    }
</script>
@endsection
    <div class="container-fluid" >
        <div class="maps">
            <iframe src="https://maps.google.com/maps?q=place/Street+579,+Phnom+Penh&amp;t=&amp;z=13&amp;ie=UTF8&amp;iwloc=&amp;output=embed" width="100%" height="320" frameborder="0" allowfullscreen="allowfullscreen" style="border: 0px;"></iframe>
        </div>
    </div>
    <div class="section section-contents section-contact section-pad  ">
        <div class="container">
            <div class="content row"  >
                <h2 class="heading-lg contact-us">Contact Us</h2>
                <div class="contact-content row" id="submit-contacted">
                    <div class="drop-message col-md-8 res-m-bttm">
                        <p>Want to work with us or need more details about consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolor.</p>
                        @if(Session::has('msg'))
                        <div class="alert alert-success" role="alert">
                            {{ Session::get('msg') }}
                        </div>
                        @endif @if (count($errors) > 0)
                        <div class="alert alert-danger" role="alert">
                            Sending fail! Please Try again!
                        </div>
                        @endif @if (count($errors) > 0) @foreach ($errors->all() as $error)
                        <div class="alert alert-danger" role="alert">
                            {{ $error }}
                        </div>
                        @endforeach @endif
                        <form id="submit-contact" class="form-quote" action="{{ route('sendmessage') }}" method="post">
                            {{ csrf_field() }} {{ method_field('PUT') }}
                            <div class="form-group row">
                                <div class="form-field col-md-6 form-m-bttm">
                                    <input name="name" id="name" type="text" placeholder="Name *">
                                </div>
                                <div class="form-field col-md-6">
                                    <input name="company" id="company" type="company" placeholder="Company *">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="form-field col-md-6 form-m-bttm">
                                    <input name="email" id="email" placeholder="Email *">
                                </div>
                                <div class="form-field col-md-6">
                                    <input name="phone" id="phone" type="text" placeholder="Phone *">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="form-field col-md-12">
                                    <textarea name="message" id="message" placeholder="Messages *" class="txtarea form-control required"></textarea>
                                </div>
                            </div> 
                            <div class="form-group row">
                                <div class="form-field col-md-4">
                                    <input type="submit" class="btn" placeholder="Submit"> 
                                </div>
                            </div>  
                        </form>
                    </div>
                    <div class="wgs-box wgs-contact-info col-md-4">
                        <h3 class="wgs-heading">Contact Information</h3>
                        <div class="wgs-content boxed">
                            <ul class="contact-list">
                                <li><em class="fa fa-map" aria-hidden="true"></em>
                                    <span>CG06, St.579 Sk. Chroy Changva,<br>Kh. Chroy Changva, Phnom Penh</span>
                                </li>
                                <li><em class="fa fa-phone" aria-hidden="true"></em>
                                    <span>Phone: +(855) 16 949 294</span>
                                </li>
                                <li><em class="fa fa-envelope" aria-hidden="true"></em>
                                    <span>Email : <a style="color:#828282" href="#">info@edvise.asia </a></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8S3pD12gYeCiM2vUf5MuObhznkGbWNCk"></script>
    <script src="{{ asset('public/frontend/js/map.js')}}"></script>
    <script src="{{ asset('public/frontend/js/map-2.js')}}"></script>

@endsection