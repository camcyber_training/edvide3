<!-- Content Section -->
<!-- Banner/Slider -->
	@extends('frontend.layouts.master')

@section('title', 'Welcome to Edvise')
@section('active-activities', 'current')
@section ('content')
<div class="banner banner-static">
    <div class="banner-cpn">
        <div class="container">
            <div class="content row">
                <div class="banner-text">
                </div>
            </div>
        </div>
    </div>
    <div class="banner-bg imagebg">
        <img src=" {{asset (getAboutbanner('Career'))}} " alt="" />
    </div>
</div>
<div class="section section-contents section-pad">
    <div class="container">
        <div class="row content ">
            <div class="news">
                <h2 class="heading-lg contact-us ">Careers</h2>
                <p>Here at Edvise, we're always looking for talented, passionate and ambitious candidates to join our growing team. We encourage you to submit your application at any time, even if your desired position is not listed in our current job openings. If we think that your qualifications and experience match with what we are looking for, we will be contacting you immediately. If you're interested, please submit your CV with your most recent photo and cover letter.</p>
                <div class="news-list-view">
                  @foreach($careers as $row)
                    <div class="job articletype-0">
                        <a href="{{ route('careers-detail', $row->id ) }}"><h3 class="font-text">{{ $row->title }}</h3>
						<div class="teaser-text">
							<p class="bodytext">{{ $row->description }}</p></div>
							<span class="news-list-date">
								<p>Posted : {{ $row->post }}</p>
								<p>Closing Date: {{ $row->closing }}</p>
							</span></a>
                    </div>
                  @endforeach
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 pagintions ">
            {{ $careers->links('vendor.pagination.frontend-html') }}
            </div>
        </div>
    </div>
</div>
@endsection