@extends('frontend.layouts.master')

@section('title', 'Welcome to Edvise')
@section('active-director-speeche', 'current')


@section ('content')
<div class="banner banner-static">
			<div class="banner-cpn">
				<div class="container">
					<div class="content row">
						<div class="banner-text">						
						</div>
					</div>
				</div>
			</div>
			<div class="banner-bg imagebg">
				<img src=" {{asset (getAboutbanner('Director Speech'))}} " alt="" />
			</div>
</div>
		<!-- #end Banner/Static -->
		</header>
		<!-- End Header -->
		<!-- Content Section -->
		<div class="section section-contents section-pad" style="padding-bottom:70px;">
			<div class="container">
				@foreach ( $director as $row)
				<div class="content row">
					<div class="portfolio-details row">
						<div class="col-sm-6 res-m-bttm">
							<h2> {{ $row->title}} </h2>
							<p> {!! $row->contents !!} </p>
						</div>

						<div class="col-sm-6">
							<img style="width: 400px;"src="{{ asset( $row->image) }}" alt="">
							<div class="gaps size-md"></div>
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>		
			</div>
		</div>
		<!-- End Section -->
		<!-- Client logo -->

		<div class="section section-logos section-pad-sm bg-light bdr-top">
			<div class="container">
				<div class="content row">

					<div class="owl-carousel loop logo-carousel style-v2">
						@foreach( $partner as $row)
						<div class="logo-item"><img alt="" width="190" height="82" src="{{ asset( $row ->image) }} "></div>

						@endforeach
					</div>

				</div>
			</div>
		</div>	
			</div>
		</div>
		<!-- End Section -->

		<!-- Call Action -->
	@foreach( $bannerfooter as $row)
	<div class="call-action cta-small has-bg bg-primary" style="background-image: url('{{ asset( $row->image) }}');">
		<div class="cta-block">
			<div class="container">
				<div class="content row">

						<div class="cta-sameline">
							<h2>{{ $row->title}}</h2>
							<p>{{$row->contents}}</p>
							<a class="btn btn-alt" href="{{ route('contact') }}">Contact Us</a>
						</div>

					</div>
				</div>
			</div>
		</div>
	@endforeach
	
@endsection