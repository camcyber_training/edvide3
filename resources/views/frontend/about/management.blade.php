@extends('frontend.layouts.master')

@section('title', 'Welcome to Edvise')
@section('active-director-speeche', 'current')


@section ('content')

<div class="banner banner-static">
			<div class="banner-cpn">
				<div class="container">
					<div class="content row">
						<div class="banner-text">						
						</div>
					</div>
				</div>
			</div>
			<div class="banner-bg imagebg">
				<img src=" {{asset (getAboutbanner('Management Team'))}} " alt="" />
			</div>
</div>
		
		<!-- #end Banner/Static -->
	</header>
	<!-- End Header -->
	<!-- Content Section -->
	<div class="section section-contents section-pad">
		<div class="container">
			<div class="content row">
			
				<div class="row">
					<div class="col-md-8 team-bottom">
					
						@foreach($management as $row)
						<div class="team-profile">
							<div class="team-member row">								
								<div class="team-photo col-md-4 col-sm-5 col-xs-12">
									<img alt="" src="{{ asset( $row->image ) }}">
								</div>
								<div class="team-info col-md-8 col-sm-7 col-xs-12">
									<h3 class="name">{{$row->name}}</h3>
									<p class="sub-title">{{ $row->title}}</p>
									<p class="teams-font">{!! $row->contents !!}</p>
								</div>
							</div>
						</div>
						@endforeach	
					</div>
				</div>	
			</div>
		</div>		
	</div>
	<!-- End Section -->
	<!-- Client logo -->
	<div class="section section-logos section-pad-sm bg-light bdr-top">
		<div class="container">
			<div class="content row">
				<div class="owl-carousel loop logo-carousel style-v2">
				@foreach( $partner as $row)
            <div class="logo-item"> <a href="{{ $row->url}}"><img alt="" width="190" height="82" src="{{ asset( $row ->image) }} "></a></div>
            @endforeach
				</div>

			</div>
		</div>	
	</div>
	<!-- End Section -->

	<!-- Call Action -->
	@foreach( $bannerfooter as $row)
	<div class="call-action cta-small has-bg bg-primary" style="background-image: url('{{ asset( $row->image) }}');">
		<div class="cta-block">
			<div class="container">
				<div class="content row">

					<div class="cta-sameline">
						<h2>{{ $row->title}}</h2>
						<p>{{$row->contents}}</p>
						<a class="btn btn-alt" href="{{ route('contact') }}">Contact Us</a>
					</div>

				</div>
			</div>
		</div>
	</div>
	@endforeach

	<!-- End Section -->
	@endsection