@extends('frontend.layouts.master')

@section('title', 'Welcome to Edvise')
@section('active-Our Clients Say', 'current')


@section ('content')


<div class="banner banner-static">
			<div class="banner-cpn">
				<div class="container">
					<div class="content row">
						<div class="banner-text">						
						</div>
					</div>
				</div>
			</div>
			<div class="banner-bg imagebg">
				<img src=" {{asset (getAboutbanner('Our Client Say'))}} " alt="" />
			</div>
</div>
	</header>
	<!-- End Header -->

	<!-- Content -->
	<div class="section section-contents section-pad">
		<div class="container">
			<div class="row">

				<div class="wide-md">
					<div class="testimonials testimonials-list">
						
					@foreach( $ourclient as $row)
					<div class="quotes"style="margin-bottom: 20px;">
					<div class="profile"style="padding-bottom: 35px;">
								<h5>{{ $row->name }}</h5>
								<h6>
								{{ $row->title }}
								</h6>
							</div>
							<div class="quotes-text">
								<p><i style="font-size: 16px;">
								{!! $row->contents !!}
								</i></p>	
							</div>		
					</div>
					@endforeach
				</div>

			</div>
		</div>
	</div>
	<!-- End Content -->	
	<!-- Client logo -->
	<div class="section section-logos section-pad-sm bg-light bdr-top">
		<div class="container">
			<div class="content row">
  		    	<div class="owl-carousel loop logo-carousel style-v2">
				  @foreach( $partner as $row)
            <div class="logo-item"> <a href="{{ $row->url}}"><img alt="" width="190" height="82" src="{{ asset( $row ->image) }} "></a></div>
            @endforeach
				</div>

			</div>
		</div>	
	</div>
	<!-- End Section -->

	<!-- Call Action -->
	@foreach( $bannerfooter as $row)
	<div class="call-action cta-small has-bg bg-primary" style="background-image: url('{{ asset( $row->image) }}');">
		<div class="cta-block">
			<div class="container">
				<div class="content row">

					<div class="cta-sameline">
						<h2>{{ $row->title}}</h2>
						<p>{{$row->contents}}</p>
						<a class="btn btn-alt" href="{{ route('contact') }}">Contact Us</a>
					</div>

				</div>
			</div>
		</div>
	</div>
	@endforeach

	<!-- End Section -->
	@endsection