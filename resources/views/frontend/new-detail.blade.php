@extends('frontend.layouts.master')

@section('title', 'Welcome to Edvise')
@section('active-director-speeche', 'current')


@section ('content')
	
	<div class="section section-contents section-pad">
	<div class="section section-blog section-pad">
		<div class="container">
			<div class="content row">
			
				<div class="blog-wrapper row">
					<div class="col-md-8 res-m-bttm">
							
						<div class="post post-single">
							<div class="post-thumbs">
								<img alt="" src="{{ asset( $detail->image) }}">
							</div>
							<div class="post-meta">
								<span class="pub-date"><em class="fa fa-calendar" aria-hidden="true"></em> {{ Carbon\Carbon::parse($detail->created_at)->format('d') }}-{{ Carbon\Carbon::parse($detail->created_at)->format('F') }}-{{ Carbon\Carbon::parse($detail->created_at)->format('Y') }}</span>
							</div>
							<div class="post-entry">
							{!! $detail->contents !!}
							</div>
						</div>	
					</div>

					<!-- Sidebar -->
					<div class="col-md-4">
						<div class="sidebar-right">
							<!-- <div class="wgs-box wgs-search">
								<div class="wgs-content">
									<div class="form-group">
										<input type="text" class="form-control"  placeholder="Search...">
										<button class="search-btn"><i class="fa fa-search" aria-hidden="true"></i></button>
									</div>
								</div>
							</div> -->
							<div class="wgs-box wgs-recents">
								<h3 class="wgs-heading">LATEST NEWS</h3>
								<div class="wgs-content">
									<ul class="blog-recent">
									@foreach( $ourupdate as $row )
										<li>
											<a href="{{ route('new-detail',$row->id) }}" style="text-decoration: none;" >
												<img alt="" src="{{ asset( $row->image) }}">
												<p>{{ $row->title}}</p>
											</a>
										</li>
										@endforeach	
									</ul>
								</div>	
							</div>

							<div class="wgs-box wgs-contact-info">
								<h3 class="wgs-heading">Contact Information</h3>
								<div class="wgs-content boxed"style="margin-bottom: 30px;">
									<ul class="contact-list">
									<li><em class="fa fa-map" aria-hidden="true"></em>
								<span>CG06, St.579 Sk. Chroy Changva,<br>Kh. Chroy Changva, Phnom Penh</span>
							</li>
							<li><em class="fa fa-phone" aria-hidden="true"></em>
								<span>Phone: +(855) 16 949 294</span>
							</li>
							<li><em class="fa fa-envelope" aria-hidden="true"></em>
								<span>Email : <a style="color: #333333;" href="#">info@edvise.asia </a></span>
							</li>
							
									</ul>
								</div>
							</div>

						</div>
					</div>
				
				</div>

			</div>
		</div>
	</div>
	</div>
	
	
@endsection