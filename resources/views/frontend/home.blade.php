@extends('frontend.layouts.master')
 @section('title', 'Welcome to Edvise')
  @section('active-home', 'current')
  
  @section ('content')

    <!-- =========================slide -->
    <div id="slider" class="banner banner-slider carousel slide carousel-fade">
        <div class="carousel-inner">
            @php($i = 1) @foreach ($slide as $row)
            <div class="item @if($i++ ==1) active @endif">
                <div class="fill" style="background-image:url('{{ asset( $row->image) }}');">
                    <div class="banner-content">
                        <div class="container">
                            <div class="row">
                                <div class="banner-text al-left pos-left dark">
                                    <h2>{{ $row->name }}<strong></strong></h2>
                                    <p>{{ $row->title }}</p>
                                    <a href="{{$row->url}}" class="btn">Learn more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <a class="left carousel-control" href="#slider" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#slider" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!-- ==========================endslide -->

    <!-- Service Section -->
	<div class="section section-services">
	    <div class="container">
	        <div class="content row">
				<!-- Feature Row  -->
				<div class="feature-row feature-service-row row feature-s4 off-text boxed-filled boxed-w">
					<div class="heading-box clearfix">
						<div class="col-sm-3">
							<h2 class="heading-section">Financial Specialists</h2>
						</div>
						<div class="col-sm-8 col-sm-offset-1">
							<span>Years of knowledge, along with care and attention brings with us the greatest results for our clients.</span>
						</div>
					</div>
                    
                    @foreach ( $defaultData['services'] as $row)
					<div class="col-md-3 col-sm-6 col-xs-6 even">
						<!-- Feature box -->
						<div class="feature">
							<a href="{{ route('services', $row->slug) }}">
								<div class="fbox-photo">
                                <img src="{{ asset( $row->image) }}" alt="">
								</div>
								<div class="fbox-over">
									<h3 class="title">{{ $row->title }}</h3>
									<div class="fbox-content">
										{{-- <p>{{ $row->description }}</p> --}}
										{{-- <span class="btn">Learn More</span> --}}
									</div>
								</div>
							</a>
						</div>
						<!-- End Feature box -->
					</div>
                    @endforeach
				</div>
				<!-- Feture Row  #end -->
	        </div>
	    </div>
	</div>
    <!-- End Section -->
    
    <!-- Content -->
	<div class="section section-content section-pad" style="    margin-bottom: 100px;">
		<div class="container">
			<div class="content row">

				<div class="row row-vm">
					<div class="col-md-6 res-m-bttm">
						<h5 class="heading-sm-lead">About us</h5>
						<h2 class="heading-section">{{ $whoweare->title ?? ''}}</h2>
						<p>{{ $whoweare->contents ?? ''}}</p>
						
					</div>  
					<div class="col-md-5 col-md-offset-1">
						<img class="no-round" src="{{ asset ($whoweare->image ?? '') }}" alt="">
					</div>
				</div>
				
			</div>	
        </div>
        
	</div>
    <!-- End Content -->
    <div class="call-action cta-small has-bg bg-primary  " style="background-image: url({{ asset('public/frontend/assets/image/plx-cta.jpg') }});">
		<div class="cta-block">
			<div class="container">
				<div class="content row">

					<div class="cta-sameline">
						<h2>Have any Question?</h2>
						<p>We're here to help. Send us an email or call us at +(855) 16 949 294. Please feel free to contact our expert.</p>
						<a class="btn btn-alt" href="{{ route('contact') }}">Contact Us</a>
					</div>

				</div>
			</div>
		</div>
	</div>

    <div class="section section-teams section-pad bdr-bottom">
        <div class="container">
            <div class="content row">
                <div class="col-md-offset-2 col-md-8 center">
                    <h5 class="heading-sm-lead">The Team</h5>
                    <h2 class="heading-section">Our Management Team</h2>
                </div>
                <div class="gaps"></div>
                <div class="team-member-row row">
                    @foreach( $ourmanagement as $row)
                    <div class="col-md-3 col-sm-6 col-xs-6 even">
                        <!-- Team Profile -->
                        <div class="team-member">
                            <div class="team-photo">
                                <img alt="" src="{{ asset ($row->image) }} ">
                            </div>
                            <div class="team-info center">
                                <h3 class="name">{{ $row->title }}</h3>
                                <p class="sub-title">{{ $row->contents }}</p>
                            </div>
                        </div>
                        <!-- Team #end -->
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="section section-content section-pad bdr-bottom">
		<div class="container">
			<div class="content row">
        		<div class=" wide-sm  center">
					<h1>Why Choosing Edvise?</h1>
					<p>Our goal is to draw upon research and experience from throughout our professional services organization, and that of coauthors in academia and business laudantium inventore veritatis et quasi architecto beatae.</p>
				</div>
	
				<div class="feature-row feature-service-row row feature-s5">
                @foreach ($why as $row)
					<div class="col-sm-4 col-xs-12 even">
						<div class="feature">
							<a href="#">
								<div class="fbox-photo">
									<img src="{{ asset($row->image) }}" alt="">
								</div>
								<div class="fbox-over">
									<h3 class="title">{{ $row->name }}</h3>
									<div class="fbox-content">
										<p>{{ $row->title }}</p>
									</div>
								</div>
							</a>
						</div>
					</div>
                @endforeach	
				</div>
			</div>	
		</div>
	</div>
    <!-- End Section -->
    <!-- Latest News -->
    <div class="section section-news section-pad">
        <div class="container">
            <div class="content row">
                <h2 class="heading-section center"> 
                    New Posts
                </h2>
                <div class="row">
                    <!-- Blog Post Loop -->
                    <div class="blog-posts">
                        @foreach( $ourupdate as $row )
                        <div class="post post-loop  col-md-4">
                            <div class="post-thumbs">
                                <a href="{{ route('new-detail',$row->id) }}"><img alt="" src="{{ asset( $row->image) }}"></a>
                            </div>
                            <div class="post-entry">
                                <div class="post-meta"><span class="pub-date">{{ Carbon\Carbon::parse($row->created_at)->format('d') }}-{{ Carbon\Carbon::parse($row->created_at)->format('F') }}-{{ Carbon\Carbon::parse($row->created_at)->format('Y') }}</span></div>
                                <h2><a href="{{ route('new-detail',$row->id) }}" style="text-decoration: none;" >{{ $row->title}}</a></h2>
                                <!-- <p>{{ $row->description}}</p> -->
                                <a class="btn btn-alt" href="{{ route('new-detail', $row->id ) }}">Read More</a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Section -->
    <!-- Partner logo -->
    <div class="section section-logos section-pad-sm bdr-top">
        <div class="owl-carousel loop logo-carousel style-v2">
            @foreach( $partner as $row)
            <div class="logo-item"> <a href="{{ $row->url}}"><img alt="" width="190" height="82" src="{{ asset( $row ->image) }} "></a></div>
            @endforeach
        </div>
    </div>
    <!-- End Section -->

@endsection