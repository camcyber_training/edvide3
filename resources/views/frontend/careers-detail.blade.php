<!-- Content Section -->
<!-- Banner/Slider -->
	@extends('frontend.layouts.master')

@section('title', 'Welcome to Edvise')
@section('active-activities', 'current')


@section ('content')

<div class="banner banner-static">
    <div class="banner-cpn">
        <div class="container">
            <div class="content row">
                <div class="banner-text">
                </div>
            </div>
        </div>
    </div>
    <div class="banner-bg imagebg">
        <img src=" {{asset (getAboutbanner('Activites'))}} " alt="" />
    </div>
</div>

<div class="container whole-content" id="main-content">
    <div class="row-fluid">
        <div class="span9">
            <div class="span12">
                <ul class="breadcrumb">
                    <li class="hidden">Compliance Officer-AML/CFT</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="span9" id="left_content">
            <div class="news news-single">
                <div class="header-text">
                    <h2>{{ $detail->title}}</h2>
                </div>
                <p><span class="news-list-date">Posted: {{$detail->post}} -Closing Date: {{$detail->closing}}</span></p>
                <div class="news-text-wrap">
                    {!! $detail->contents !!}
                </div>
            </div>
        </div>
    </div>
    <div class="section section-contents section-contact">
        <div class="container">
            <div class="content row">
                <h3 class="job-header">Submit Application Form</h3>
                <div class="contact-content row" id="submit-contacted">
                    <div class="drop-message col-md-8 res-m-bttm">
                        <p class="interested">Interested and qualified applicants should submit only their updated covering letter and CV stating the position you apply for with current photo (4x6) via the Application form below.</p>
                        @if(Session::has('msg'))
                        <div class="alert alert-success" role="alert">
                            {{ Session::get('msg') }}
                        </div>
                        @endif @if (count($errors) > 0)
                        <div class="alert alert-danger" role="alert">
                            Sending fail! Please Try again!
                        </div>
                        @endif @if (count($errors) > 0) @foreach ($errors->all() as $error)
                        <div class="alert alert-danger" role="alert">
                            {{ $error }}
                        </div>
                        @endforeach @endif
                        <form id="submit-contact" class="form-quotes" action="{{ route('send-career') }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }} {{ method_field('PUT') }}
                            <input name="careers_id" id="careers_id" type="text" hidden value="{{$detail->id}}">
                            <div class="form-group row">
                                <div class="form-field col-md-4 form-m-bttm">
                                    <input name="name" id="name" type="text" placeholder="Name *">
                                </div>
                                <div class="form-field col-md-4 form-m-bttm">
                                    <input name="email" id="email" placeholder="Email *">
                                </div>
                                <div class="form-field col-md-4">
                                    <input name="phone" id="phone" type="text" placeholder="Phone *">
                                </div>
                            </div>
                            <div class="form-group row control-left  ">
                                <div class="col-md-6 col-xs-12">
                                    <div class="controls">
                                        <input class="input-file btn float-left " type="file" id="file" name="file" value="">
                                        <!-- <span class="info"> pdf, doc, docx, jpeg, gif, zip</span> -->
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-field col-md-2 submit-form">
                                    <input type="submit" class="btn" placeholder="Submit">
                                </div>
                            </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
