<!DOCTYPE html>
<html lang="zxx">

<head>
<title>Welcome to Edvise</title>
	<meta charset="utf-8" />
	<link rel="shortcut icon" href="{{ asset ('public/frontend/assets/favicon.png') }}" type="image/x-icon">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="icon" href="{{ asset('public/frontend/assets/image/favicon.png" type="image/png" sizes="16x16') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/frontend/assets/css/vendor.bundle.css') }}">
	<link id="style-css" rel="stylesheet" type="text/css" href="{{ asset('public/frontend/assets/css/stylec64ec64e.css?ver=1.1.1') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/frontend/assets/css/style.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/frontend/assets/camcyber/customise.css') }}">
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5d542f3beb1a6b0be6078734/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>

</head>
<body class="site-body style-v1">
	<!-- Header --> 
	<header class="site-header header-s1 is-sticky">
		<!-- Topbar -->
		<div class="topbar">
			<div class="container">
				<div class="row">
					<div class="top-aside top-left">
						<ul class="top-nav">
							<li class="t-marker ">
								<a href="#" >
								<em class="fa fa-map-marker" style="font-size: 18px;"aria-hidden="true"></em>
								<span class="t-markers">CG06, St.579 Sk. Chroy Changva, Kh. Chroy Changva, Phnom Penh</span> </a></li>
						</ul>
					</div>
					<div class="top-aside top-right clearfix">
						<ul class="top-contact clearfix">
							<li class="t-email t-email1">
								<em class="fa fa-envelope-o" aria-hidden="true"></em>
								<span><a href="mailto:info@edvise.asia">info@edvise.asia</a></span>
							</li>
							<li class="t-phone t-phone1">
								<em class="fa fa-phone" aria-hidden="true"></em>
								<span> <a href="tel:85516949294">+(855) 16 949 294</a></span>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- #end Topbar -->
		<!-- Navbar -->
		<div class="navbar navbar-primary">
			<div class="container">
				<!-- Logo -->
				<a class="navbar-brand" href="{{ route('home') }}">
					<img class="logo logo-dark" alt="" src="{{ asset('public/frontend/assets/camcyber/logo.png') }}" srcset="{{ asset('public/frontend/assets/camcyber/logo.png') }}">
					<img class="logo logo-light" alt="" src="{{ asset('public/frontend/assets/camcyber/logo-light.png') }}" srcset="{{ asset('public/frontend/assets/camcyber/logo-light.png') }}">
				</a>
				<!-- #end Logo -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mainnav" aria-expanded="false">
						<span class="sr-only">Menu</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<!-- MainNav -->
				<nav class="navbar-collapse collapse" id="mainnav">
				<ul class="nav navbar-nav">
						<li class="dropdown @yield('active-home')"><a href="{{ route('home') }}">Home</a>
						</li>
						<li class="dropdown @yield('active-about-us')"><a href="#">About us <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<a href="{{ route('director-speech')}}">Director's Speech</a>
								<a href="{{ route('management')}}">Management Team</a>
								<!-- <a href="#">Our Value</a> -->
								<a href="{{ route('our-client')}}">Our Clients Say</a>
							</ul>
						</li>
						<li class="dropdown @yield('active-services')">
							<a href="#" class="dropdown-toggle">Services<b class="caret"></b></a>
							<ul class="dropdown-menu">
								@foreach($defaultData['services'] as $row)
									<a href="{{ route('services', $row->slug) }}">{{$row->title}}</a>
								@endforeach	
							</ul>
						</li>
						<li  class="@yield('active-careers')"><a href="{{ route('career') }}">Careers</a></li>
						<li  class="@yield('active-activities')"><a href="{{ route('activities') }}">Activities</a></li>
						<li  class="quote-btn @yield('active-contact')"><a class="btn" href="{{ route('contact') }}">Contact Us</a></li>
					</ul>
				</nav>     
				<!-- #end MainNav -->
			</div>
		</div>
	@yield('content')
	<!-- Footer Widget-->
	<div class="footer-widget style-v2 section-pad-md">
		<div class="container">
			<div class="row">

				<div class="widget-row row">
					<div class="footer-col col-md-3 col-sm-6 res-m-bttm">
						<!-- Each Widget -->
						<div class="wgs wgs-footer wgs-text">
							<div class="wgs-content">
								<p><img src="{{ asset('public/frontend/assets/camcyber/logo.png') }}"  alt=""></p>
								<p>Edvise is founded by a group of young and dynamic Cambodian entrepreneurs who have
									more than a decade of experiences in education, travelling & visa arrangement and
								career counseling. </p>
							</div>
						</div>
						<!-- End Widget -->
					</div>
					<div class="footer-col col-md-3 col-sm-6 col-md-offset-1 res-m-bttm" style="margin-top: 45px;">
						<!-- Each Widget -->
						<div class="wgs wgs-footer wgs-menu">
							<h5 class="wgs-title">Our Services</h5>
							<div class="wgs-content">
								<ul class="menu" >
								@foreach($defaultData['services'] as $row)
								<li><a href="{{ route('services', $row->slug) }} "style="text-decoration: none;">{{$row->title}}</a></li>
								@endforeach
									
								</ul>
							</div>
						</div>
						<!-- End Widget -->
					</div>
					<div class="footer-col col-md-2 col-sm-6 res-m-bttm"style="margin-top: 45px;">
						<!-- Each Widget -->
						<div class="wgs wgs-footer wgs-menu">
							<h5 class="wgs-title">About Us</h5>
							<div class="wgs-content">
								<ul class="menu">
									<li><a href="{{ route('director-speech')}}"style="text-decoration: none;">Director's Speech</a></li>
									<li><a href="{{ route('management')}}"style="text-decoration: none;">Management Team</a></li>
								</ul>
							</div>
						</div>
						<!-- End Widget -->
					</div>

					<div class="footer-col col-md-3 col-sm-6"style="margin-top: 45px;">
						<!-- Each Widget -->
						<div class="wgs wgs-footer">
							<h5 class="wgs-title">Get In Touch</h5>
							<div class="wgs-content">
								<p>CG06, St.579 Sk. Chroy Changva, Kh. Chroy Changva, Phnom Penh</p>
								<p>
									<span>Phone</span>: +(855) 16 949 294<br>
									<span>E-mail</span>: info@edvise.asia <br />
									<span>Website</span>: www.edvise.asia
								</p>
								<ul class="social">
									<li><a href="https://www.facebook.com/edvise.asia/"><em class="fa fa-facebook" aria-hidden="true"></em></a></li>
									<li><a href="#"><em class="fa fa-twitter" aria-hidden="true"></em></a></li>
									<li><a href="#"><em class="fa fa-linkedin" aria-hidden="true"></em></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="copyright style-v2">
		<div class="container">
			<div class="row">
				<div class="row">
					<div class="site-copy col-sm-7">
						<p>Copyright &copy; {{ Date('Y') }} Edvise Consulting Co., LTD. All Rights Reserved
					</div>
					<div class="site-by col-sm-5 al-right">
						<p>Designed by <a href="#" target="_blank">CamCyber</a></p>
					</div>
				</div>
				 				
			</div>
		</div>
	</div>
	<!-- End Copyright -->
	
	<!-- Rreload Image for Slider -->
	<div class="preload hide">
		<img alt="" src="{{ asset('public/frontend/assets/image/slider-lg-a.jpg') }}">
		<img alt="" src="{{ asset('public/frontend/assets/image/slider-lg-b.jpg') }}">
	</div>
	<!-- End -->

	<!-- Preloader !active please if you want -->
	<!-- <div id="preloader"><div id="status">&nbsp;</div></div> -->
	<!-- Preloader End -->

	<!-- JavaScript Bundle -->
	<script src="{{ asset('public/frontend/assets/js/jquery.bundle.js') }}"></script>
	<!-- Theme Script init() -->
	<script src="{{ asset('public/frontend/assets/js/script.js') }}"></script>
	<!-- End script -->
</body>

</html>