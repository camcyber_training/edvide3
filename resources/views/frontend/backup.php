@extends('frontend.layouts.master') @section('title', 'Welcome to Edvise') @section('active-home', 'current') @section ('content')
<!-- =========================slide -->
    <div id="slider" class="banner banner-slider carousel slide carousel-fade">
                <div class="carousel-inner">
                @php($i = 1)
                    @foreach ($slide as $row)
                    <div class="item @if($i++ ==1) active @endif">
                        <div class="fill" style="background-image:url('{{ asset( $row->image) }}');">
                            <div class="banner-content">
                                <div class="container">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>   
                <a class="left carousel-control" href="#slider" role="button" data-slide="prev">
                    <span  aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#slider" role="button" data-slide="next">
                    <span  aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

    <!-- ==========================endslide -->
    <!-- Service Section -->

    <div class="section section-content section-pad">
            <div class="container">
                <div class="content row">
                    <div class="wide-sm center">
                        <h1>Constulting Expert</h1>
                        <p class="constulting">Years of knowledge, along with care and attention brings with us the greatest results for our clients.</p>
                    </div>
                    <!-- Feature Row  -->
                
                    <div class="feature-row feature-service-row row feature-s5">
                    @foreach ( $constulting as $row)
                        <div class="col-sm-4 col-xs-12 even">
                            <!-- Feature box -->
                            <div class="feature">
                                <a href="{{ route('constult-detail', $row->id) }}">
                                    <div class="fbox-photo">
                                        <img src="{{ asset( $row->image) }}" alt="">
                                    </div>
                                    <div class="fbox-over">
                                        <h3 class="title"> {{  $row->title }}</h3>
                                        <div class="fbox-content">
                                            <p>{{ $row->description }}</p>
                                        </div>
                                        
                                    </div>
                                </a>
                            </div>
                            <!-- End Feature box -->
                        </div>
                        @endforeach
                    </div>
                </div>	
            </div>
        </div>
    <!-- End Section -->

    <!-- Content -->

    <div class="section section-content section-pad">
        @foreach( $whoweare as $row)
        <div class="container">
            <div class="content row">

                <div class="row row-vm">
                    <div class="col-md-6 res-m-bttm">
                        <h5 class="heading-sm-lead">About us</h5>
                        <h2 class="heading-section">{{ $row->title }}</h2>
                        <p>{{ $row->contents }}</p>
                    </div>
                    <div class="col-md-5 col-md-offset-1">
                        <img class="no-round" src="{{ asset ($row->image) }}" alt="">
                    </div>
                </div>

            </div>
        </div>
        @endforeach
    </div>

    <div class="section section-contents section-pad image-on-right bg-light" style="padding-bottom:60px;">
        <div class="container">
            <div class="row">
                <h5 class="heading-sm-lead">Our Services</h5>
                <h2 class="heading-section">What we do</h2>
                <div class="feature-intro">
                    <div class="row">
                        <div class="col-sm-7 col-md-6">
                            <div class="row">
                                @foreach ($whatwedo as $row)
                                <div class="col-sm-6"style="padding-top: 20px;padding-bottom: 20px;" >
                                    <div class="icon-box style-s4 photo-plx-full" >
                                    <img src="{{ asset($row->image) }} " alt="">
                                    </div>
                                        <h4>{{ $row->name }}</h4>
                                        <p style="font-size:15px;line-height: 1.60;"> {{ $row->title }} </p>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-bg imagebg"><img src="{{ asset('public/frontend/assets/image/photo-half-a.jpg') }} " alt=""></div>
    </div>
    <!-- End Content -->
    <!-- Content -->
    <div class="section section-contents section-pad has-bg fixed-bg light bg-alternet">
        <div class="container">
            <div class="row">

                <div class="row">
                    <div class="col-md-4 pad-r res-m-bttm">
                        <h2 class="heading-lead">Why choosing Edvise?</h2> 
                    </div>
                    <div class="col-md-8" >
                        <div class="row">
                          @foreach ($why as $row)
                            <div class="col-sm-6 res-s-bttm"style="padding-bottom:40px;">
                                <div class="icon-box style-s4 photo-plx-full">
                                <img src="{{ asset ($row->image) }}  " alt="">
                                </div>
                                <h4>{{ $row->name }}</h4>
                                <p style="font-size:14px;">{{ $row->title }}</p>
                            </div>
                          @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-bg imagebg"><img src="{{ asset('public/frontend/assets/image/plx-full.jpg') }} " alt=""></div>
    </div>
    <!-- End Content -->
    <!-- Teams -->
    <div class="section section-teams section-pad bdr-bottom" style="padding-bottom: 90px;">
        <div class="container">
            <div class="content row">

                <div class="col-md-offset-2 col-md-8 center">
                    <h5 class="heading-sm-lead">The Team</h5>
                    <h2 class="heading-section">Our Management Team</h2>
                </div>
                <div class="gaps"></div>
                
                <div class="team-member-row row">
                    @foreach( $ourmanagement as $row)
                    <div class="col-md-3 col-sm-6 col-xs-6 even">
                        <!-- Team Profile -->
                        <div class="team-member">
                            <div class="team-photo">
                                <img alt="" src="{{ asset ($row->image) }} ">
                            </div>
                            <div class="team-info center">
                                <h3 class="name">{{ $row->title }}</h3>
                                <p class="sub-title" style="font-size:14px;">{{ $row->contents }}</p>
                            </div>
                        </div>
                        <!-- Team #end -->
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!-- End Section -->
    <!-- Latest News -->
    <div class="section section-news section-pad">
        <div class="container">
            <div class="content row">

                <h5 class="heading-sm-lead center">Latest News</h5>
                <h2 class="heading-section center">Our Updates</h2>

                <div class="row">
                    <!-- Blog Post Loop -->
                    <div class="blog-posts">
                        @foreach( $ourupdate as $row )
                        <div class="post post-loop  col-md-4">
                            <div class="post-thumbs">
                                <a href="{{ route('new-detail',$row->id) }}"><img alt="" src="{{ asset( $row->image) }}"></a>
                            </div>
                            <div class="post-entry">
                                <div class="post-meta"><span class="pub-date">{{ Carbon\Carbon::parse($row->created_at)->format('d') }}-{{ Carbon\Carbon::parse($row->created_at)->format('F') }}-{{ Carbon\Carbon::parse($row->created_at)->format('Y') }}</span></div>
                                <h2><a href="{{ route('new-detail',$row->id) }}" style="text-decoration: none;" >{{ $row->title}}</a></h2>
                                <p>{{ $row->description}}</p>
                                <a class="btn btn-alt" href="{{ route('new-detail', $row->id ) }}">Read More</a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- End Section -->
    <!-- Client logo -->
    <div class="section section-logos section-pad-sm bg-light bdr-top">
        <div class="owl-carousel loop logo-carousel style-v2">
            @foreach( $partner as $row)

            <div class="logo-item"><img alt="" width="190" height="82" src="{{ asset( $row ->image) }} "></div>

            @endforeach
        </div>
    </div>
    <!-- End Section -->

    <!-- Call Action -->
    @foreach( $bannerfooter as $row)
    <div class="call-action cta-small has-bg bg-primary" style="background-image: url('{{ asset( $row->image) }}');">
        <div class="cta-block">
            <div class="container">
                <div class="content row">

                    <div class="cta-sameline">
                        <h2>{{ $row->title}}</h2>
                        <p >{{$row->contents}}</p>
                        <a class="btn btn-alt" href="{{ route('contact') }}">Contact Us</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endforeach @endsection