<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Slide

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'ActivityController@index']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'ActivityController@create']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'ActivityController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'ActivityController@update']);
	
	Route::put('/', 				['as' => 'store', 			'uses' => 'ActivityController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'ActivityController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'ActivityController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'ActivityController@updateStatus']);

	///================= Image 

	Route::get('/{id}/image', 			['as' => 'image', 			'uses' => 'ActivityImageController@index']);

	Route::get('/{id}/create-image', 			['as' => 'create-image', 			'uses' => 'ActivityImageController@create']);
	Route::put('/{id}/create-image', 				['as' => 'store-image', 			'uses' => 'ActivityImageController@store']);

	Route::get('/{id}/edit-image/{image_id}', 			['as' => 'edit-image', 			'uses' => 'ActivityImageController@edit']);
	Route::post('/{id}/update-image/{image_id}', 				['as' => 'update-image', 			'uses' => 'ActivityImageController@update']);

	Route::delete('/{id}/trash-image/{image_id}', 			['as' => 'trash-image', 			'uses' => 'ActivityImageController@trash']);
	Route::post('/{id}/order-image', 			['as' => 'order-image', 			'uses' => 'ActivityImageController@order']);
	Route::post('{id}/status-image', 			['as' => 'update-status-image', 	'uses' => 'ActivityImageController@updateStatus']);
});	