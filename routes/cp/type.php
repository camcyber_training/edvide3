<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Service 

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'TypeController@index']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'TypeController@create']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'TypeController@store']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'TypeController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'TypeController@update']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'TypeController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'TypeController@order']);
	

	
});	