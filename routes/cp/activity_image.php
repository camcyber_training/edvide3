<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Service 

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'ActivityImageController@index']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'ActivityImageController@create']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'ActivityImageController@store']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'ActivityImageController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'ActivityImageController@update']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'ActivityImageController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'ActivityImageController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'ActivityImageController@updateStatus']);

	
});	