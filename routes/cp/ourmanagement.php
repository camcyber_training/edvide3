<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Slide

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'OurmanagementController@index']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'OurmanagementController@create']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'OurmanagementController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'OurmanagementController@update']);
	
	Route::put('/', 				['as' => 'store', 			'uses' => 'OurmanagementController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'OurmanagementController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'OurmanagementController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'OurmanagementController@updateStatus']);
});	