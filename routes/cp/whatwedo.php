<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Slide

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'WhatwedoController@index']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'WhatwedoController@create']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'WhatwedoController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'WhatwedoController@update']);
	
	Route::put('/', 				['as' => 'store', 			'uses' => 'WhatwedoController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'WhatwedoController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'WhatwedoController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'WhatwedoController@updateStatus']);
});	