<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Slide

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'CareersController@index']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'CareersController@create']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'CareersController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'CareersController@update']);
	
	Route::put('/', 				['as' => 'store', 			'uses' => 'CareersController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'CareersController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'CareersController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'CareersController@updateStatus']);


	Route::get('/{id}/apply', 			['as' => 'apply', 			'uses' => 'CareersapplyController@index']);

	Route::get('/{id}/edit-apply/{apply_id}', 			['as' => 'edit-apply', 			'uses' => 'CareersapplyController@edit']);
	Route::post('/{id}/update-apply/{apply_id}', 				['as' => 'update-apply', 			'uses' => 'CareersapplyController@update']);

	Route::delete('/{id}/trash-apply/{apply_id}', 			['as' => 'trash-apply', 			'uses' => 'CareersapplyController@trash']);
	Route::post('/{id}/order-apply', 			['as' => 'order-apply', 			'uses' => 'CareersapplyController@order']);
	Route::post('{id}/status-apply', 			['as' => 'update-status-apply', 	'uses' => 'CareersapplyController@updateStatus']);
});	