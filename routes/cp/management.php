<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Slide

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'ManagementController@index']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'ManagementController@create']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'ManagementController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'ManagementController@update']);
	
	Route::put('/', 				['as' => 'store', 			'uses' => 'ManagementController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'ManagementController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'ManagementController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'ManagementController@updateStatus']);
});	