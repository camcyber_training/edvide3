<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Slide

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'BannerfooterController@index']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'BannerfooterController@create']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'BannerfooterController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'BannerfooterController@update']);
	
	Route::put('/', 				['as' => 'store', 			'uses' => 'BannerfooterController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'BannerfooterController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'BannerfooterController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'BannerfooterController@updateStatus']);
});	