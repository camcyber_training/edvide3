<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Slide

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'WhyController@index']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'WhyController@create']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'WhyController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'WhyController@update']);
	
	Route::put('/', 				['as' => 'store', 			'uses' => 'WhyController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'WhyController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'WhyController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'WhyController@updateStatus']);
});	