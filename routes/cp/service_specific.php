<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Service 

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'ServiceSpecificController@index']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'ServiceSpecificController@create']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'ServiceSpecificController@store']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'ServiceSpecificController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'ServiceSpecificController@update']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'ServiceSpecificController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'ServiceSpecificController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'ServiceSpecificController@updateStatus']);
});	